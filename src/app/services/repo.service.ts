import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Repo } from '../models/repo';
import { Issue } from '../models/issue';
import { ApiService } from './api.service';

@Injectable()
export class RepoService {
    constructor(private api: ApiService) {
        
    }

    getRepos(filter?: string): Observable<Repo[]> {
        let endPoint = '/repositories';
        return this.api.get(endPoint).map(res => res.json() as Repo[]).catch(err => Observable.throw(err));
    }

    search(q: string): Observable<any> {
        let endPoint = '/search/repositories?q='+q+'&sort=score&order=desc';
        return this.api.get(endPoint).map(res => res.json()).catch(err => Observable.throw(err));

    }
     
    getIssues(repo: Repo): Observable<Issue[]> {
        let endPoint = '/search/issues?q=repo:' + repo.full_name + '&sort=created_at&order=desc';
        return this.api.get(endPoint).map(res => res.json().items as Issue[]).catch(err => Observable.throw(err));
    }
}
