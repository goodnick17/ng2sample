import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    @Input()  title: string;
    @Output() onKeyup = new EventEmitter<string>();
    onSearch(input: string) {
      this.onKeyup.emit(input);
    }
    constructor() { }

    ngOnInit() {
    }
}
