import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { Repo } from '../../models/repo';
import { Issue } from '../../models/issue';

import { RepoService } from '../../services/repo.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    public title = 'GitHub Search Sample';
    public repos: Repo[] = [];
    public search: Subject<string> = new Subject<string>();
    public minSearchTextLenght = 3;
    public selectedRepo: Repo = new Repo();
    public searchText: string;
    public emptySearchbox: boolean = true;
    public loadingIssues: boolean = false;   
    public barChartData = [
        ['Category', 'Count'],
        ['Open issues', 0],
        ['Forks', 0],
        ['Stargazers', 0],
        ['Watchers', 0]
    ];          
    public barChartOptions = {
        title: '',
        chartArea: {width: '60%'},
        hAxis: {
            title: '',
            minValue: 0,
            textStyle: {
                bold: true,
                fontSize: 12,
                color: '#4d4d4d'
            },
            titleTextStyle: {
                bold: true,
                fontSize: 12,
                color: '#4d4d4d'
            }
        },
        vAxis: {
            title: 'Category',
            textStyle: {
                fontSize: 12,
                bold: true,
                color: '#848484'
            },
            titleTextStyle: {
                fontSize: 12,
                bold: true,
                color: '#848484'
            }
        }
    };
    public gaugeChartOptions = {
          width: '100%', height: '100%',
          greenFrom: 90, 
          greenTo: 100,
          greenColor: '#00ff40',
          yellowFrom:75, 
          yellowTo: 90,
          yellowColor: '#bfff00',
          redFrom: 50, 
          redTo: 75,
          redColor: '#ffff00',
          minorTicks: 5
    };
    
    public constructor(public repoService: RepoService) {
        this.search.debounceTime(300).distinctUntilChanged().subscribe((searchTerm) => {
            this.repoService.search(searchTerm).subscribe(res => {
                this.repos = res.items as Repo[];
            });
        })
    }
    
    public ngOnInit() {}

    public onSearch(q: string) {
        this.searchText = q;
        this.selectedRepo = new Repo();
        this.repos = [];
        if (this.searchText !== "") {
            this.emptySearchbox = false
            if(this.searchText.length >= this.minSearchTextLenght){
                this.search.next(this.searchText);
            }
        } else {
            this.emptySearchbox = true;
        }
    }

    public home() {
        this.selectedRepo.selected = false;
        this.selectedRepo = new Repo();
    }

    public selectRepo(repo: Repo) {
        this.selectedRepo = repo;
        this.selectedRepo.selected = true;
        this.loadingIssues = true;
        this.repoService.getIssues(repo).subscribe(res => {
            this.selectedRepo.issues = res;
            this.loadingIssues = false;
            this.initBarChart(this.selectedRepo);
        }, err => {
            console.log(err);
            this.loadingIssues = false;
        });
    }

    public initBarChart(repo){
        this.barChartData = [
               ['Category', 'Count'],
               ['Open issues', repo.open_issues_count],
               ['Forks', repo.forks],
               ['Stargazers', repo.stargazers_count],
               ['Watchers', repo.watchers]
        ]; 
    }
    
    public gaugeData(repo){
        return [
            ['Label', 'Value'],
            ['Popularity', this.getPopularityIndex(repo)],
            ['Diversity', this.getDiversityIndex(repo)],
            ['Load', this.getLoadIndex(repo)],
            ['Maintainance', this.getMaintainIndex(repo)]
        ];
    }
    
    public getPopularityIndex(repo){
        return ( (Math.round(repo.watchers/1000) > 100) ? 100 : Math.round(repo.watchers/1000) );
    }
    
    public getDiversityIndex(repo){
        return ( (Math.round(repo.forks/10) > 100) ? 100 : Math.round(repo.watchers/1000) );
    }
    
    public getLoadIndex(repo){
        return ( (Math.round(repo.open_issues_count) > 100) ? 100 : Math.round(repo.watchers/1000) );
    }
    
    public getInactiveDays(repo){
        let now = new Date();
        let pushed = new Date(repo.pushed_at);
        return  Math.floor((now.getTime() - pushed.getTime()) / 86400000);
    }
    
    public getInactiveDaysSentence(repo){
        let inactive_days = this.getInactiveDays(repo);
        if(inactive_days == 0){
            return 'Today';
        }else if (inactive_days < 2) {
            return 'Yesturday';
        }else {
            return inactive_days +' days ago';
        }
    }
    
    public getMaintainIndex(repo){
        let inactive_days = this.getInactiveDays(repo);
        return ( (Math.round(100-(inactive_days/6)) > 0) ? Math.round(100-(inactive_days/6)) : 0 );
    }
}