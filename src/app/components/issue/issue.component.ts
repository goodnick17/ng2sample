import { Component, OnInit, Input } from '@angular/core';
import { Issue } from '../../models/issue';

@Component({
    selector: 'app-issue',
    templateUrl: './issue.component.html',
    styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {
    @Input() issue;
    
    
    constructor() { }

    ngOnInit() {
    }
    
    public viewIssue(issue: Issue) {
        window.open(issue.html_url);
    }
}
