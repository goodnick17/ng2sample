import { Component, OnInit, EventEmitter, Input, Output  } from '@angular/core';
import { Repo } from '../../models/repo';

@Component({
  selector: 'app-repo-details',
  templateUrl: './repo-details.component.html',
  styleUrls: ['./repo-details.component.css']
})
export class RepoDetailsComponent implements OnInit {
    @Input() repo;
    @Output() onSelectRepo = new EventEmitter();
    selectRepo(repo: Repo) {
      this.onSelectRepo.emit(repo);
    }
    
    constructor() { }

    ngOnInit() {
    }
    
    public getInactiveDays(repo){
        let now = new Date();
        let pushed = new Date(repo.pushed_at);
        return  Math.floor((now.getTime() - pushed.getTime()) / 86400000);
    }
    
    public getInactiveDaysSentence(repo){
        let inactive_days = this.getInactiveDays(repo);
        if(inactive_days == 0){
            return 'Today';
        }else if (inactive_days < 2) {
            return 'Yesturday';
        }else {
            return inactive_days +' days ago';
        }
    }
    
    public getMaintainIndex(repo){
        let inactive_days = this.getInactiveDays(repo);
        return ( (Math.round(100-(inactive_days/6)) > 0) ? Math.round(100-(inactive_days/6)) : 0 );
    }

}
