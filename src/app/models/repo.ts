import {Issue} from "./issue";

export class Repo {
    public id?: string;
    public name?: string;
    public full_name?: string;
    public owner?: Object;
    public private?: boolean;
    public html_url?: string;
    public description?: string;
    public fork?: boolean;
    public url?: string;
    public forks_url?: string;
    public keys_url?: string;
    public collaborators_url?: string;
    public teams_url?: string;
    public hooks_url?: string;
    public issue_events_url?: string;
    public events_url?: string;
    public assignees_url?: string;
    public branches_url?: string;
    public tags_url?: string;
    public blobs_url?: string;
    public git_tags_url?: string;
    public git_refs_url?: string;
    public trees_url?: string;
    public statuses_url?: string;
    public languages_url?: string;
    public stargazers_url?: string;
    public contributors_url?: string;
    public subscribers_url?: string;
    public subscription_url?: string;
    public commits_url?: string;
    public git_commits_url?: string;
    public comments_url?: string;
    public issue_comment_url?: string;
    public contents_url?: string;
    public compare_url?: string;
    public merges_url?: string;
    public archive_url?: string;
    public downloads_url?: string;
    public issues_url?: string;
    public pulls_url?: string;
    public milestones_url?: string;
    public notifications_url?: string;
    public labels_url?: string;
    public releases_url?: string;
    public deployments_url?: string;
    public created_at?: string;
    public updated_at?: string;
    public pushed_at?: string;
    public git_url?: string;
    public ssh_url?: string;
    public clone_url?: string;
    public svn_url?: string;
    public homepage?: string;
    public size?: number;
    public stargazers_count?: number;
    public watchers_count?: number;
    public language?: string;
    public has_issues?: boolean;
    public has_downloads?: boolean;
    public has_wiki?: boolean;
    public has_pages?: boolean;
    public forks_count?: number;
    public mirror_url?;
    public open_issues_count?: number;
    public forks?: number;
    public open_issues?: number;
    public watchers?: number;
    public default_branch?: string;
    public score?: number;

    public issues?: Issue[];
    public selected: boolean = false;
}
/*
 * {
      "id": 2126244,
      "name": "bootstrap",
      "full_name": "twbs/bootstrap",
      "owner": {
        "login": "twbs",
        "id": 2918581,
        "avatar_url": "https://avatars.githubusercontent.com/u/2918581?v=3",
        "gravatar_id": "",
        "url": "https://api.github.com/users/twbs",
        "html_url": "https://github.com/twbs",
        "followers_url": "https://api.github.com/users/twbs/followers",
        "following_url": "https://api.github.com/users/twbs/following{/other_user}",
        "gists_url": "https://api.github.com/users/twbs/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/twbs/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/twbs/subscriptions",
        "organizations_url": "https://api.github.com/users/twbs/orgs",
        "repos_url": "https://api.github.com/users/twbs/repos",
        "events_url": "https://api.github.com/users/twbs/events{/privacy}",
        "received_events_url": "https://api.github.com/users/twbs/received_events",
        "type": "Organization",
        "site_admin": false
      },
      "private": false,
      "html_url": "https://github.com/twbs/bootstrap",
      "description": "The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.",
      "fork": false,
      "url": "https://api.github.com/repos/twbs/bootstrap",
      "forks_url": "https://api.github.com/repos/twbs/bootstrap/forks",
      "keys_url": "https://api.github.com/repos/twbs/bootstrap/keys{/key_id}",
      "collaborators_url": "https://api.github.com/repos/twbs/bootstrap/collaborators{/collaborator}",
      "teams_url": "https://api.github.com/repos/twbs/bootstrap/teams",
      "hooks_url": "https://api.github.com/repos/twbs/bootstrap/hooks",
      "issue_events_url": "https://api.github.com/repos/twbs/bootstrap/issues/events{/number}",
      "events_url": "https://api.github.com/repos/twbs/bootstrap/events",
      "assignees_url": "https://api.github.com/repos/twbs/bootstrap/assignees{/user}",
      "branches_url": "https://api.github.com/repos/twbs/bootstrap/branches{/branch}",
      "tags_url": "https://api.github.com/repos/twbs/bootstrap/tags",
      "blobs_url": "https://api.github.com/repos/twbs/bootstrap/git/blobs{/sha}",
      "git_tags_url": "https://api.github.com/repos/twbs/bootstrap/git/tags{/sha}",
      "git_refs_url": "https://api.github.com/repos/twbs/bootstrap/git/refs{/sha}",
      "trees_url": "https://api.github.com/repos/twbs/bootstrap/git/trees{/sha}",
      "statuses_url": "https://api.github.com/repos/twbs/bootstrap/statuses/{sha}",
      "languages_url": "https://api.github.com/repos/twbs/bootstrap/languages",
      "stargazers_url": "https://api.github.com/repos/twbs/bootstrap/stargazers",
      "contributors_url": "https://api.github.com/repos/twbs/bootstrap/contributors",
      "subscribers_url": "https://api.github.com/repos/twbs/bootstrap/subscribers",
      "subscription_url": "https://api.github.com/repos/twbs/bootstrap/subscription",
      "commits_url": "https://api.github.com/repos/twbs/bootstrap/commits{/sha}",
      "git_commits_url": "https://api.github.com/repos/twbs/bootstrap/git/commits{/sha}",
      "comments_url": "https://api.github.com/repos/twbs/bootstrap/comments{/number}",
      "issue_comment_url": "https://api.github.com/repos/twbs/bootstrap/issues/comments{/number}",
      "contents_url": "https://api.github.com/repos/twbs/bootstrap/contents/{+path}",
      "compare_url": "https://api.github.com/repos/twbs/bootstrap/compare/{base}...{head}",
      "merges_url": "https://api.github.com/repos/twbs/bootstrap/merges",
      "archive_url": "https://api.github.com/repos/twbs/bootstrap/{archive_format}{/ref}",
      "downloads_url": "https://api.github.com/repos/twbs/bootstrap/downloads",
      "issues_url": "https://api.github.com/repos/twbs/bootstrap/issues{/number}",
      "pulls_url": "https://api.github.com/repos/twbs/bootstrap/pulls{/number}",
      "milestones_url": "https://api.github.com/repos/twbs/bootstrap/milestones{/number}",
      "notifications_url": "https://api.github.com/repos/twbs/bootstrap/notifications{?since,all,participating}",
      "labels_url": "https://api.github.com/repos/twbs/bootstrap/labels{/name}",
      "releases_url": "https://api.github.com/repos/twbs/bootstrap/releases{/id}",
      "deployments_url": "https://api.github.com/repos/twbs/bootstrap/deployments",
      "created_at": "2011-07-29T21:19:00Z",
      "updated_at": "2017-01-14T07:43:54Z",
      "pushed_at": "2017-01-14T08:01:14Z",
      "git_url": "git://github.com/twbs/bootstrap.git",
      "ssh_url": "git@github.com:twbs/bootstrap.git",
      "clone_url": "https://github.com/twbs/bootstrap.git",
      "svn_url": "https://github.com/twbs/bootstrap",
      "homepage": "http://getbootstrap.com",
      "size": 95423,
      "stargazers_count": 105718,
      "watchers_count": 105718,
      "language": "JavaScript",
      "has_issues": true,
      "has_downloads": true,
      "has_wiki": false,
      "has_pages": true,
      "forks_count": 48030,
      "mirror_url": null,
      "open_issues_count": 303,
      "forks": 48030,
      "open_issues": 303,
      "watchers": 105718,
      "default_branch": "v4-dev",
      "score": 136.62296
    }
 */