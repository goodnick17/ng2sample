export class Issue {
    public url?: string;
    public repository_url?: string;
    public labels_url?: string;
    public comments_url?: string;
    public events_url?: string;
    public html_url?: string;
    public id?: number;
    public number?: number;
    public title?: string;
    public user?: Object;
    public labels? Array;
    public state?: string;
    public locked?: boolean;
    public created_at?: string;
    public updated_at?: string;
    public closed_at?: string;
    public pull_request?: Object;
    public body?: string;
    public score?: number;
}
/*
 * {
      "url": "https://api.github.com/repos/twbs/bootstrap/issues/21722",
      "repository_url": "https://api.github.com/repos/twbs/bootstrap",
      "labels_url": "https://api.github.com/repos/twbs/bootstrap/issues/21722/labels{/name}",
      "comments_url": "https://api.github.com/repos/twbs/bootstrap/issues/21722/comments",
      "events_url": "https://api.github.com/repos/twbs/bootstrap/issues/21722/events",
      "html_url": "https://github.com/twbs/bootstrap/pull/21722",
      "id": 200789038,
      "number": 21722,
      "title": "Fix container with in navbar on smallest breakpoint",
      "user": {
        "login": "vanduynslagerp",
        "id": 3250552,
        "avatar_url": "https://avatars.githubusercontent.com/u/3250552?v=3",
        "gravatar_id": "",
        "url": "https://api.github.com/users/vanduynslagerp",
        "html_url": "https://github.com/vanduynslagerp",
        "followers_url": "https://api.github.com/users/vanduynslagerp/followers",
        "following_url": "https://api.github.com/users/vanduynslagerp/following{/other_user}",
        "gists_url": "https://api.github.com/users/vanduynslagerp/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/vanduynslagerp/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/vanduynslagerp/subscriptions",
        "organizations_url": "https://api.github.com/users/vanduynslagerp/orgs",
        "repos_url": "https://api.github.com/users/vanduynslagerp/repos",
        "events_url": "https://api.github.com/users/vanduynslagerp/events{/privacy}",
        "received_events_url": "https://api.github.com/users/vanduynslagerp/received_events",
        "type": "User",
        "site_admin": false
      },
      "labels": [

      ],
      "state": "open",
      "locked": false,
      "assignee": null,
      "assignees": [

      ],
      "milestone": null,
      "comments": 0,
      "created_at": "2017-01-14T08:01:13Z",
      "updated_at": "2017-01-14T08:01:13Z",
      "closed_at": null,
      "pull_request": {
        "url": "https://api.github.com/repos/twbs/bootstrap/pulls/21722",
        "html_url": "https://github.com/twbs/bootstrap/pull/21722",
        "diff_url": "https://github.com/twbs/bootstrap/pull/21722.diff",
        "patch_url": "https://github.com/twbs/bootstrap/pull/21722.patch"
      },
      "body": "Fixes #21605 and #21590\r\n\r\nAt the smallest breakpoint the `container` has no `with` set. In the case of a `container` within a `navbar`, due to `margin-right: auto` and `margin-left: auto` the container content is coalesced toward the center.\r\n\r\nThis PR set `margin-right: 0` and `margin-left: 0` for `container` within `navbar` to avoid the issue.",
      "score": 1.0
    }
 */