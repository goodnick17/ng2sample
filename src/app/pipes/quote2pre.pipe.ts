import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'quote2pre'})
export class Quote2PrePipe implements PipeTransform {
  transform(value: string): string {
    return value.replace(
        new RegExp("\`([^\`]*)\`", 'g'), 
        function myFunction(m){
            return '<code>'+m
                .replace(/\`/g, '')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                +'</code>';
        }
    );
   }
}
