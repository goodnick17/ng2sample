import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'nl2br'})
export class Nl2BrPipe implements PipeTransform {
  transform(value: string): string {
    return value
        .replace(new RegExp("\\r\\n", 'g'), "<br />")
        .replace(new RegExp("\\t", 'g'), "&nbsp;&nbsp;&nbsp;&nbsp;");
  }
}
