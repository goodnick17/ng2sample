import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'image2img'})
export class Image2ImgPipe implements PipeTransform {
    transform(value: string): string {
        let exp = /!?\[image\]\((\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])\)/ig;
        return value.replace(exp, "<img src='$1' />");
    }
}
