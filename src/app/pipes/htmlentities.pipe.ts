import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'htmlentities'})
export class HtmlEntitiesPipe implements PipeTransform {
    transform(value: string): string {
        return value
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;');
    }
}
