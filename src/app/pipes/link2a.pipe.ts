import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'link2a'})
export class Link2APipe implements PipeTransform {
    transform(value: string): string {
        let exp = /!?\[([-A-Z 0-9+&@#\/%?=~_|!:,.;\(\)?!\[]*)\]\((\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])\)/ig;
        return value.replace(exp, "<a href='$2' target='_blank'>$1</a>");
    }
}
