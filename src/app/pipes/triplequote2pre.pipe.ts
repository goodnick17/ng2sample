import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'triplequote2pre'})
export class TripleQuote2PrePipe implements PipeTransform {
  transform(value: string): string {
    return value.replace(
        new RegExp("\`\`\`([^\`]*)\`\`\`", 'g'), 
        function myFunction(m){
            return '<pre class="pre-scrollable">'+m.replace(/\`\`\`/g, '')+'</pre>';
        }
    );
   }
}
