
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './components/app/app.component';
import { ApiService } from "./services/api.service";
import { RepoService } from "./services/repo.service";

import { FocusDirective } from "./directives/focus.directive";
import { HighlightDirective } from "./directives/highlight.directive";

import { HtmlEntitiesPipe } from './pipes/htmlentities.pipe';
import { Nl2BrPipe } from './pipes/nl2br.pipe';
import { Link2APipe } from './pipes/link2a.pipe';
import { Image2ImgPipe } from './pipes/image2img.pipe';
import { TripleQuote2PrePipe } from './pipes/triplequote2pre.pipe';
import { Quote2PrePipe } from './pipes/quote2pre.pipe';

import { GoogleChartComponent } from './components/google-chart/google-chart.component';
import { FooterComponent } from './components/footer/footer.component';
import { IssueComponent } from './components/issue/issue.component';
import { RepoDetailsComponent } from './components/repo-details/repo-details.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { TabsModule } from "ng2-tabs";

@NgModule({
    declarations: [
        AppComponent,
        FocusDirective,
        HighlightDirective,
        HtmlEntitiesPipe,
        Nl2BrPipe,
        Link2APipe,
        Image2ImgPipe,
        TripleQuote2PrePipe,
        Quote2PrePipe,
        GoogleChartComponent,
        FooterComponent,
        IssueComponent,
        RepoDetailsComponent,
        NavbarComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        TabsModule
    ],
    providers: [
        ApiService, 
        RepoService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }